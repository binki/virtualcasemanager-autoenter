// ==UserScript==
// @id             virtualcasemanager-autoenter
// @name           Virtual Case Automatic Entry
// @version        1.10
// @namespace      ohnopub.net
// @author         Nathan Phillip Brink
// @description    Automatically enter things into the virtual case manager.
// @include        https://virtualcasemanager.net/clients/overview.aspx*
// @include        https://virtualcasemanager.net/clients/CompleteEDocument.aspx*
// @include        https://virtualcasemanager.net/user/BarcodeScan.aspx*
// @include        https://virtualcasemanager.net/default.aspx*
// @require        https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js
// @grant          GM_setValue
// @grant          GM_getValue
// @run-at         document-end
// ==/UserScript==
(function (jQuery) {
  var stor_get = function (key, def) {
    try
    {
      return JSON.parse(GM_getValue(key, JSON.stringify(def)));
    }
    catch (e)
    {
      return def;
    }
  };
  var stor_set = function (key, val) {
    GM_setValue(key, JSON.stringify(val));
  };

  var chrome = function(content) {
    jQuery('body').prepend(
      jQuery(
        '<div/>',
        {
          style: 'border: 3pt solid gold;'
        }).append(
          jQuery('<h2/>', { text: 'Virtual Case Automatic Entry' })).append(content));
  };

  var humans_parse = function(humans_text, error_text_obj) {
    var humans_errored = false;
    var line_num = 0;
    var humans = jQuery.map(
      humans_text.split('\n'),
      function (human_text) {
        line_num++;
        if (false
            || humans_errored
            || !human_text.length)
          return;

        var human_parts = human_text.split('\t');
        var expected_human_parts_length = 6;
        if ((humans_errored = (human_parts.length != expected_human_parts_length)))
        {
          error_text_obj.text('Unable to understand line ' + line_num + ' (“' + human_text + '”), there are ' + human_parts.length + ' human parts instead of ' + expected_human_parts_length + '.');
          return;
        }
        var human_qty = parseInt(human_parts[3] || 0, 10);
        if ((humans_errored = isNaN(human_qty)))
        {
          error_text_obj.text('Unable to understand line ' + line_num + ' (“' + human_text + '”), third column (“' + human_parts[2] + '”) is not blank or a valid number.');
          return;
        }

        return {
          barcode: human_parts[0],
          lastName: human_parts[1],
          firstName: human_parts[2],
          formattedName: human_parts[1] + ', ' + human_parts[2],
          quantity: human_qty,
          source: human_parts[4],
          service: human_parts[5]
        };
      });
    if (humans_errored)
      return;
    return humans;
  };
  var humans_deparse = function (humans) {
    return jQuery.map(
      humans,
      function (human) {
        return [
          human.barcode,
          human.lastName,
          human.firstName,
          human.quantity,
          human.source,
          human.service
        ].join('\t');
      }).join('\n');
  };
  /**
   * \brief
   *   Take a humans array and return a new one which has skipped
   *   the current one.
   */
  var humans_skipfirst = function (humans) {
    return humans.slice(1).concat(humans[0])
  };

  var navigate_to_client_lookup = function () {
    location.href = jQuery('#ctl00_divMenuSearch > a').prop('href');
  };

  jQuery(document).ready(
    function () {
	    /* batch autoentry. */
      var is_client_search_page = /\/\/[^\/]+\/[Dd]efault\.aspx($|[?#])/.test(location.href);
      var is_entry_page = !is_client_search_page && /\/\/[^\/]+\/clients\/(CompleteEDocument|overview)\.aspx($|[?#])/.test(location.href);

	    /* quick provide */
	    var is_quick_provide_search_page = /\/\/[^\/]+\/user\/BarcodeScan\.aspx\?Clear=1/.test(location.href);
	    var is_quick_provide_entry_page = /\/\/[^\/]+\/user\/BarcodeScan\.aspx\?(.*&|)C=/.test(location.href);

      if (is_client_search_page)
      {
        /*
         * Let the service page know that it’s going to get a
         * new human…
         */
        var service = stor_get('service');
        if (service)
          /* Purposefully exclude lastCase/lastCaseCount keys! */
          stor_set('service', {source: service.source, service: service.service});

        var go_auto;
        var humans_text = stor_get('humans', '');

        /*
         * Display chrome on client search page.
         */
        var textarea_obj;
        var go_obj;
        var cancel_obj;
        var counter_obj;
        chrome([
          textarea_obj = jQuery('<textarea/>', { id: 'vcm-auto-humans', text: humans_text, style: 'width: 30em; height: 15em;' }),
          go_obj = jQuery('<button/>', { text: 'Go' }),
          cancel_obj = jQuery('<button/>', { text: 'Cancel', type: 'button' }),
          counter_obj = jQuery('<span/>', { id: 'vcm-auto-counter' }),                    
        ]);

        /*
         * Do not automatically try the next human if the user
         * updated the textarea.
         */
        var docancel = function () {
          stor_set('service', {});
          if (go_auto)
            counter_obj.text('Cancelled automatic go, please click Go.');
          go_auto = false;
        };
        var waitTime = 2;
        var goauto = function () {
          go_auto = true;
          setTimeout(
            function () {
              if (go_auto)
                go();
            }, waitTime * 1000);
        };
        textarea_obj.on('input', docancel);
        cancel_obj.click(docancel);

        var go = function () {
          humans_text = textarea_obj.val();
          stor_set('humans', humans_text);
		      stor_set('humans_go_timestamp', Date.now());

          var humans = humans_parse(humans_text, counter_obj);

          if (!humans)
            /* error during parsing */
            return;
          if (!humans.length)
          {
            counter_obj.text('Out of humans.');
            return;
          }

          var human = humans[0];
          counter_obj.text('Found ' + humans.length + ' humans.');

          /* Skip even looking up the human if it has no quantity. */
          if (!human.quantity)
          {
            textarea_obj.val(humans_deparse(humans.slice(1)));
            counter_obj.text(counter_obj.text() + ' Skipped human ' + human.formattedName + '. Moving on in ' + waitTime + ' seconds…');
            goauto();
            return;
          }

          var txtSearchLastName_obj = jQuery('input[name$=txtSearchLastName]')
          if (txtSearchLastName_obj.length != 1)
          {
            counter_obj.text('Found ' + txtSearchLastName_obj.length + ' instead of 1 places to put last names into.');
            return;
          }
          if (txtSearchLastName_obj.val())
          {
            /*
             * When we attempt a search that results in
             * nothing being found, the response returns
             * our form filled out and an error
             * message. The error message is probably
             * translatable, but we can detect this case
             * by seeing that the form is already filled
             * out.
             *
             * Handle by moving the current human to the
             * end of the human input buffer. This way we
             * can finish all of the existing humans and
             * leave a pile of nonexistent humans at the
             * end.
             */
            textarea_obj.val(humans_deparse(humans_skipfirst(humans)));
            counter_obj.text('It appears that the human “' + human.formattedName + '” does not exist in the database. Moving to end of list and skipping for now. Will continue in ' + waitTime + ' seconds…');
            txtSearchLastName_obj.val('');
            goauto();
            return;
          }
          txtSearchLastName_obj.val(human.barcode);
          /* submit */
          jQuery('*[name$=btnSearchSubmit]').click();
        };

        go_obj.click(go);
        goauto();
        counter_obj.text('Will do something in ' + waitTime + ' seconds.');

		    /*
		     * Cancel if we arrive at this page more than 10
		     * minutes after the last goauto(). This is so that a
		     * user who comes back to the browser after being gone
		     * overnight or whatever doesn’t inadvertently enter a
		     * lot of stale things, though that is a bad situation
		     * to begin with because it means the user never
		     * finished an earlier run…
		     */
		    if (Math.abs(stor_get('humans_go_timestamp', 0) - Date.now()) > 10*60*1000)
		    {
		      docancel();
		      counter_obj.text(counter_obj.text() + ' 10 minute timeout.');
		    }
      }

      if (is_entry_page)
      {
        var is_edocument_page = /^[^?]*\/CompleteEDocument\.aspx/.test(location.href);

        var go_auto;
        var waitTime = 2;

        var message_obj;
        var go_obj;
        var cancel_obj;
        chrome([
          go_obj = jQuery('<button/>', {text: 'Go', type: 'button'}),
          cancel_obj = jQuery('<button/>', {text: 'Cancel', type: 'button'}),
          jQuery('<br/>'),
          message_obj = jQuery('<span/>', {text: 'Messages will appear here.'}),
        ]);
        var docancel = function () {
          if (go_auto)
            message_obj.text('Automatic Go disabled, to continue verify source/service and press Go.');
          go_auto = false;
        };
        cancel_obj.click(docancel);

        var goauto = function () {
          go_auto = true;
          setTimeout(function () {
            if (go_auto)
              go_obj.click();
          }, waitTime * 1000);
          message_obj.text(message_obj.text() + ' Doing stuff in ' + waitTime + ' seconds.');
        };

        var humans = humans_parse(stor_get('humans', ''), message_obj);
        if (!humans)
          return;
        var human = humans[0];
        if (!human)
        {
          message_obj.text('Out of humans.');
          return;
        }
        message_obj.text('Operating on human “' + human.formattedName + '” with barcode of “' + human.barcode + '”');
        /*
         * Verify that the client we are viewing has a
         * matching barcode. Start by loading the barcode from
         * the barcode image’s URI.
         */
        var found_human_barcode_src = jQuery('#ctl00_ContentPlaceHolder1_uxFamilyDetails_imgBarcode').css('image-rendering', 'crisp-edges').attr('src');
        if (!found_human_barcode_src)
        {
          message_obj.text('Unable to find human barcode, falling back to last name match (necessary for development).');
          found_human_barcode_src = jQuery('#ctl00_ContentPlaceHolder1_uxFamilyDetails_lblName').text();
          if (!found_human_barcode_src)
          {
            message_obj.text('Unable to find human barcode or last name.');
            return;
          }
          found_human_barcode_src = '&Val=' + encodeURIComponent(/[^,]*/.exec(found_human_barcode_src)[0]);
        }
        var found_human_barcode_src_matches = /[?&]Val=([^&#]*)/.exec(found_human_barcode_src);
        if (!found_human_barcode_src_matches)
        {
          message_obj.text('Found human barcode URI “' + found_human_barcode_src + '” does not contain a valid Val GET parameter.');
          return;
        }
        var expected = human.barcode;
        var actual = decodeURIComponent(found_human_barcode_src_matches[1]);
        if (expected !== actual)
        {
          message_obj.text('Expected human with barcode “' + expected + '” but found “' + actual + '” instead. Will move human to end of list and continue.');
          go_obj.click(
            function () {
              stor_set('humans', humans_deparse(humans_skipfirst(humans)));
              navigate_to_client_lookup();
            });
          goauto();
          return;
        }
        message_obj.text(message_obj.text() + ' Verified barcode…');

        /*
         * At this point, after verifying the barcode, check
         * if we landed on an E-Document Completion page. If
         * so, skip this human and continue on.
         */
        var client_entry_success = function () {
          stor_set('humans', humans_deparse(humans.slice(1)));
          navigate_to_client_lookup();
        };
        if (is_edocument_page) {
          message_obj.text('EDocument page detected. Waiting extra time to allow screenreader to capture/export data.');
          go_obj.click(function () {
            client_entry_success();
          });

          // Bump wait time to 6s so that screen reader can do stuff.
          waitTime = 6;
          goauto();
          return;
        }

        var source_obj = jQuery('#ctl00_ContentPlaceHolder1_uxQuickProvide_ddFundingSource');
        var service_obj = jQuery('#ctl00_ContentPlaceHolder1_uxQuickProvide_ddService');
        var qty_obj = jQuery('#ctl00_ContentPlaceHolder1_uxQuickProvide_txtAmount').val(human.quantity);
        var casehistoryrows_obj = jQuery('#divServiceHistory > table > tbody > tr.fake_Row');
        var lastcasenum_obj = casehistoryrows_obj.last().children('td:nth-child(2)').css('border', '2pt solid blue');
        jQuery.map(
          [source_obj, service_obj, qty_obj],
          function (obj) {
            /* If the user updates the source/service cancel. */
            obj.on('change input', docancel).css('border', '2pt solid blue');
          });
        var input2label = function (input_obj) { return input_obj.parent().contents().first().text(); }

        var submit_obj = jQuery('#ctl00_ContentPlaceHolder1_uxQuickProvide_btnSubmit');
        setTimeout(function () {
          submit_obj.css({background: 'grey', border: '2pt solid silver'}).attr('disabled', 'disabled');
        }, 0);
        go_obj.click(function () {
          /* Read and store chosen source and service. */
          stor_set('service', {
            lastCase: lastcasenum_obj.text(),
            lastCaseCount: casehistoryrows_obj.length,
          });

          if (true && false)
          {
            message_obj.text('Would click Record Services, but debugging.');
            return;
          }
          submit_obj.removeAttr('disabled').css({background: '#005500', border: '2pt solid blue'});
          /* Let enabling take effect */
          setTimeout(function () {
            submit_obj.click();
          }, 100);
        });

        var service = stor_get('service', {}) || {};
        /*
         * After a commit, we end up back on the same page. We
         * don’t really have a way to verify that a case was
         * successfully committed(?). But we can check if the
         * last shown case number has changed. NB: this relies
         * on the client lookup page clearing out the lastCase
         * entry in the service storage key each time (because
         * if we removed it in the code now and we never
         * successfully navigated to the client page, we would
         * end up trying to double-commit the current human)!
         */
        if (false
            || true
            && service.lastCase !== undefined
            && service.lastCase !== lastcasenum_obj.text()
            || true
            && service.lastCaseCount !== undefined
            && service.lastCaseCount !== casehistoryrows_obj.length)
        {
          /*
           * Consider ourselves to have successfully
           * completed a thing. Delete the human we just
           * did. Then go look for another human.
           */
          client_entry_success();
          return;
        }
        /* Resolve the source */
        var source = source_obj.find('option').map(function () {
          var option = jQuery(this);
          if (option.text().toLowerCase() == human.source.toLowerCase())
            return option.val();
        }).get()[0];
        if (source === undefined)
        {
          message_obj.text('Unable to resolve “' + human.source + '” to ' + input2label(source_obj) + '. Please specify a valid source for this human.');
          return;
        }
        /*
         * Set the source. .change() is necessary to populate
         * the service <select/>
         */
        source_obj.val(source).change();
        setTimeout(function () {
          if (source_obj.val() !== source)
          {
            message_obj.text('Invalid ' + input2label(source_obj) + '. Please fix the things.');
            return;
          }
          var service = service_obj.find('option').map(function () {
            var option = jQuery(this);
            if (option.text().toLowerCase() == human.service.toLowerCase())
              return option.val();
          }).get()[0];
          if (service === undefined)
          {
            message_obj.text('Unable to resolve “' + human.service + '” to ' + input2label(service_obj) + '. Please specify a valid service for this human.');
            return;
          }
          service_obj.val(service).change();
          setTimeout(function () {
            if (service_obj.val() !== service)
            {
              message_obj.text('Invalid ' + input2label(service_obj) + '. Please fix the things.');
              return;
            }
            /* Yay! */
            goauto();
          }, 0);
        }, 0);
      }
	    /*
	     * If navigating outside of either batching page, halt the
	     * batching script. Easiest way is to clear the service, I
	     * guess.
	     */
	    if (!(is_entry_page
		        || is_client_search_page))
		    stor_set('service', {});

	    /* quick provide */
	    if (is_quick_provide_search_page
	        || is_quick_provide_entry_page)
	    {
		    var go = function () {};

		    var message_obj;
		    var go_obj;
		    var cancel_obj;
		    chrome([
		      go_obj = jQuery('<button/>', {type: 'button', text: 'Go'}).click(function () { return go.apply(this, arguments); }),
		      cancel_obj = jQuery('<button/>', {type: 'button', text: 'Cancel'}),
		      message_obj = jQuery('<div/>')
		    ]);
		    var provide_obj = jQuery('#ctl00_ContentPlaceHolder1_cntBarcodeScannerProvide_cmdCompleteService').css({'background-color': 'grey'}).prop('disabled', true);
		    var scan_obj = jQuery('#ctl00_ContentPlaceHolder1_cntBarcodeScannerProvide_txtScanInput');

		    /* Intercept the textbox thing’s onkeypress. */
		    var scan_onkeypress_orig;
		    var scan_onkeypress = function () {};
		    if (is_quick_provide_entry_page)
		    {
		      scan_onkeypress_orig = scan_obj[0].onkeypress;
		      scan_obj[0].onkeypress = function () {
			      var ret = scan_onkeypress();
			      if (ret !== false)
			        ret = scan_onkeypress_orig.apply(this, arguments);
			      return ret;
		      };
		    }

		    var dom2quick_provide = function () {
		      var o = {};
		      jQuery('#ctl00_ContentPlaceHolder1_cntBarcodeScannerProvide_divClientOverview').find('input[type=hidden][name^="hidQty"]').each(
			      function () {
			        var obj = jQuery(this);
			        var val = obj.val()|0;
			        if (val)
				        o[/^hidQty(.*)/.exec(obj.attr('name'))[1]] = val;
			      });
		      return o;
		    };

		    var apply_token;
		    var apply = function () {
		      var quick_provide;
		      var have_quick_provide;
		      var scan_quick_provide;

		      apply_token = {};

		      quick_provide = stor_get('quick_provide', {});
		      have_quick_provide = !!jQuery.map(quick_provide, function (q) { return q; }).length;

		      go_obj[is_quick_provide_entry_page ? 'show' : 'hide']();
		      cancel_obj[have_quick_provide ? 'show' : 'hide']();
		      message_obj.text(have_quick_provide ? 'Auto Quick Provide active.' : 'Welcome to Auto Quick Provide');

		      if (is_quick_provide_search_page)
		      {
			      /* Clear the “we submitted something” flag. */
			      stor_set('quick_provide_submitted', false);

			      if (!have_quick_provide)
			        message_obj.text('Scan a client to configure auto quick provide.');
		      }

		      if (is_quick_provide_entry_page)
		      {
			      if (stor_get('quick_provide_submitted'))
			      {
			        /* Then it’s time to go back to client scan mode. */
			        location.href = jQuery('#ctl00_divMenuInstaProvide > a').prop('href');
			        return;
			      }

			      /* Auto go or get user input? */
			      if (have_quick_provide)
			      {
			        scan_quick_provide = function (quick_provide, value) {
				        return !jQuery.each(
				          quick_provide,
				          function (key, quantity) {
					          var i;
					          for (i = 0; i < quantity; i++)
					          {
					            if (scan_obj.val())
					            {
						            message_obj.text('Confused when trying to set values.');
						            return true;
					            }
					            scan_obj.val(key + '-' + value);
					            scan_onkeypress_orig.call(scan_obj[0], {keyCode: 13});
					            if (scan_obj.val())
					            {
						            message_obj.text('Confused after trying to set a value.');
						            return true;
					            }
					          }
				          })[0];		
			        };
			        /*
			         * Undo anything that has been written out to the dom before doing anything.
			         */
			        if (!scan_quick_provide(dom2quick_provide(), '0'))
				        return;
			        if (jQuery.map(dom2quick_provide(), function (q) { return q; }).length)
			        {
				        message_obj.text('Confused: unable to clear form before writing to it.');
				        return;
			        }
			        /* Write to dom… */
			        if (!scan_quick_provide(quick_provide, '1'))
				        return;

			        /* Auto go time */
			        scan_onkeypress = function () {};
			        go = function () {
				        /* Simulate keypresses in the right spot. */
				        stor_set('quick_provide_submitted', true);

				        provide_obj.removeAttr('disabled').css({background: '#005500', border: '2pt solid blue'});
				        /* Let enabling take effect */
				        setTimeout(function () {
				          provide_obj.click();
				        }, 100);
			        };

			        message_obj.text('Doing something in 2 seconds.');
			        /*
			         * Do not pass go directly to setTimeout(). Guard with token so that cancel works.
			         */
			        setTimeout(
				        (function (apply_token_current) {
				          return function () {
					          if (apply_token_current != apply_token)
					            return;
					          return go.apply(this, arguments);
				          };
				        })(apply_token),
				        2000);
			      }
			      else
			      {
			        /* Input mode */
			        scan_onkeypress = function (e) {
				        if (e.which == 13)
				        {
				          /*
				           * If the user is configuring the
				           * autoenter using the scanner, trap the
				           * final entry but let quantity
				           * adjustments through.
				           */
				          if (scan_obj.val().indexOf('-') > 0)
				          {
					          go_obj.click();
					          return false;
				          }
				        }
			        };

			        go = function () {
				        stor_set('quick_provide', dom2quick_provide());
				        /* Reload to jump to auto behavior. */
				        document.location.reload();
				        return false;
			        };

			        message_obj.text('Select services to auto quick provide and press Go.');
			      }
		      }
		    };
		    apply();

		    cancel_obj.click(function () {
		      stor_set('quick_provide', {});
		      apply();
		    });
	    }
	    else
		    /*
		     * If navigating outside of quick provide, clear the
		     * setting for something like safety.
		     */
		    stor_set('quick_provide', {});
	    /*
	     * With quick provide, once we submit it we are just
	     * returned back to the same customer page with an option
	     * to provide again. So we must track that state
	     * ourselves.
	     */
	    stor_set('quick_provide_submitted', false);
    });
})(jQuery);
